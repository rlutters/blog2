---
title: "Schetsen maar!"
description: "Drie low-fid prototypes moeten er komen, wij kiezen er voor er vijf te maken en ze vervolgens te combineren! Ook reviews op Facebook kunnen best handig zijn."
slug: "schetsen-maar"
image: pic10.jpg
tags: ["low-fid", "user journey", "valideren"]
categories: ["prototype"] 
date: 2017-11-29
week: Week 3 | Dag 2
draft: false
---

Vandaag is dan de merkanalyse van PAARD in de template gezet, dus die is af. Afgelopen maandag is de debriefing niet gevalideerd, maar er zijn direct aanpassingen gemaakt. Ook de user journey is nu volledig digitaal, dus we kunnen door naar het volgende. 

We zijn gaan kijken naar een aantal recensies op de Facebook-pagina van PAARD om nog meer te weten te komen over de bezoekers, wat vonden ze ervan? Waarom komen ze wel of juist niet terug? 

Vervolgens moesten er drie low-fid prototypes komen. We hebben er voor gekozen elk één te maken met een totaal van vijf dus. Dit kostte aardig wat tijd. Van te voren heb ik aan de hand van alle ideeën die we al hadden aangegeven welke schermen er sowieso in moesten komen (als we spreken over een app) zodat er in ieder geval richtlijnen zijn. Daarna ging iedereen aan de slag.

Achteraf heeft iedereen zijn eigen schetsen uitgelegd in de vorm van een soort pitch en ik kwam erachter dat iedereen wel interessante ideeën en oplossingen heeft. Vervolgens is er voor gekozen om al deze ideeën te bundelen en zo een nieuw low-fid prototype te maken, een verdere benadering van het eindproduct. Deze is vandaag voor de helft afgekomen.