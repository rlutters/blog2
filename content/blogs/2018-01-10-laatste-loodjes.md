---
title: "De laatste loodjes komen in zicht!"
description: "Het is nu nog precies een week tot de expo, we moeten alles afronden en voorbereidingen gaan treffen."
slug: "laatste-loodjes"
image: pic10.jpg
tags: ["expo"] ["design rationale"] ["testplan"] ["testrapport"] ["recap"]
categories: ["afronding"]
date: 2018-01-10
week: Week 7 | Dag 3
draft: false
---
Deze dag zijn we begonnen aan de Design Rationale, deze laat zien waarom er bepaalde keuzes zijn gemaakt in het ontwerp. De schermen zijn interactief gemaakt en zo is het prototype clickable. Het testplan is gecontroleerd voor het high-fid prototype  en ik heb alvast een testrapport opgesteld. De resultaten zouden hier later in verwerkt worden. Ook is er een teamlid begonnen aan de nieuwe recap, omdat er nog een tweede gemaakt moest worden.

Daarnaast hebben we met z'n allen nagedacht wat we wilden laten zien op de expo. Het is een map van Den Haag geworden met het product daarop en een werkend clickable prototype op de tafel.