---
title: "Meer low-fid prototypes!"
description: "Bij een nieuw concept hoort natuurlijk ook weer een nieuw prototype"
slug: "meer-low-fid"
image: pic10.jpg
tags: ["low-fid prototype"]
categories: ["verbeelden en uitwerken"]
date: 2017-12-13
week: Week 5 | Dag 3
draft: false
---

Ieder van ons heeft eerst een eigen low-fid prototype gemaakt met een eigen interpretatie van het nieuwe concept dat we hadden liggen. Zo krijgen we van iedereen duidelijk te zien hoe die persoon erover denkt. Vervolgens hebben we allerlei ideeën en suggesties samen gevoegd tot een nieuw low-fid prototype waarin van iedereen wel wat terug te vinden was. Daarnaast is er een start gemaakt aan de recap van de afgelopen weken. Hiermee liepen we een beetje achter.