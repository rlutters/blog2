---
title: "Van low naar high-fid"
description: "Nu het low-fid prototype klaar is, kan deze worden omgezet in een beter uitgewerkt high-fid prototype"
slug: "begin-uitwerking"
image: pic10.jpg
tags: ["high-fid prototype"] ["styleguide"]
categories: ["uitwerking"]
date: 2017-12-20
week: Week 6 | Dag 3
draft: false
---

Vandaag heb ik mij toegewijd aan het verbeteren van de recap. Deze is laatst niet gevalideerd. Er moest meer reflectie in op het gemaakte werk. We hebben aantrekkelijke zinnen bedacht voor op het startscherm van het product en zijn hierna begonnen met het maken van deze startschermen in de huisstijl en het kleurenschema van PAARD. Van te voren is daarvoor vandaag de visual styleguide gemaakt van de club. 