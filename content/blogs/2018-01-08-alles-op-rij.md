---
title: "Alle taken om een rij"
description: "We zitten nu in de laatste paar weken, dus we moeten alles netjes zien af te ronden"
slug: "alles-op-rij"
image: pic10.jpg
tags: ["user journey"] ["high-fid prototype"] ["takenlijst"]
categories: ["afronding"]
date: 2018-01-08
week: Week 7 | Dag 1
draft: false
---
Allereerst hebben we na de kerstvakantie een lijstje gemaakt met alles wat nog gemaakt en gedaan moest worden. Dit leek in eerste instantie veel maar bleek wel mee te vallen. Er moest een user journey komen voor de toekomstige situatie. Deze taak heb ik op me genomen. Ook moest het high-fid prototype af, hiervan heb ik ook het grootste deel van de visuele schermen gemaakt en ontwerpen afgaande op de huisstijl van PAARD. 