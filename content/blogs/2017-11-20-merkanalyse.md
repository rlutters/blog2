---
title: "Hoe analyseer je een PAARD?"
description: "Tijd voor de allereerste merkanalyse. Ook moesten we nog een paar dingen afronden. Het echte werk begint hier."
slug: "analyse-paard"
image: pic07.jpg
keywords: ""
categories:
    - ""
date: 2017-11-20
draft: false
week: Week 2 | Dag 1
--- 
Gister kwamen we als team niet meer toe aan het afronden van de SWOT-analyse, dus daar zijn we vandaag mee begonnen. We hebben enkele dingen kunnen toevoegen en weten nu waar we op moeten letten en waar juist onze kansen liggen. Vervolgens ben ik, samen met een teamgenoot, gestart aan het maken van de merkanalyse van PAARD. Hiervoor heb ik eerst enkele voorbeelden opgezocht, omdat ik niet precies wist wat ervan verwacht werd. Vanuit de projectdocenten was er al een voorbeeld van Grolsch geleverd, maar ik heb er zelf nog een aantal extra opgezocht.

In de briefing heb ik gekeken wat nu echt de merkvisie van PAARD is en hoe ze deze wilden bereiken. Er stond hier beschreven hoe PAARD zich identificeert en wat de merkwaarden zijn. Deze informatie was allemaal nodig voor het invullen van de merkanalyse. Ook werd er gevraagd naar welk gevoel de PAARD moet opwekken - welke geur, klank, smaak e.d. Nadat de merkwijzer volledig was ingevuld ben ik naar een feedback-moment geweest om te vragen of de merkwijzer zo voldeed aan alle eisen. Dit was niet helemaal het geval, maar we waren goed op weg. Na enkele dingen in de wijzer aan te passen besloot ik het zo te laten. Mijn teamgenoot zal de merkwijzer binnenkort laten valideren.
