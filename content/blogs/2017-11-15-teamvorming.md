---
title: "Tijd voor nieuwe teams!"
description: "In kwartaal twee werk ik in een ander team, maar met wie? Er worden nieuwe teams gevormd en dit gaat niet bepaald soepel."
slug: "nieuwe-teams"
image: pic08.jpg
keywords: ""
categories:
    - ""
date: 2017-11-15
draft: false
---
Nog helemaal wakker was ik niet, het was immers een tijdje geleden dat ik weer zo vroeg m'n bed uit moest, maar het was tijd om teams te vormen. Dit moest gebeurd aan de hand van belbin rollen. Er waren vier categorieën: Voelen, willen, denken en doen. Het bleek dat ik viel onder voelen, dus ik sloot mij daar bij aan. Een team zou moeten bestaan uit zoveel mogelijk belbin rollen, daarom was het de bedoeling om duo's en trio's te vormen tussen voelen en willen en denken en doen. Op zo'n moment loop je maar op iemand af, maar verder dan je eigen team uit vorig kwartaal ken je niet echt iemand.

Vervolgens moesten duo's en trio's gecombineerd worden in vijftallen. Voor enkele teams ging dit vrij soepel. Voor andere teams niet, omdat zij dan weer terecht kwamen in een team met leden die zij al kende en dat was niet de bedoeling. Toen er toch een soort van teams waren gevormd, bleek dat de hele boel opnieuw moest, maar nu volgens een andere samenstelling om te zien of alles dan wel uitkwam. Gelukkig ben ik toch in een groep belandt waar ik het redelijk goed mee kan vinden.

Volgens zijn we als Team Connect begonnen met een SWOT-analyse na dat we elkaar beter hadden leren kennen. Hier kwamen we nog niet helemaal direct uit, dus we hebben deze terzijde gelegd en hebben enkele teamafspraken vastgelegd. Deze kwamen grotendeels overeen met de teamafspraken van vorig kwartaal. Hierna hebben we enkele dingen besproken om elkaar beter te leren kennen, maar ook hoe het ons vorig kwartaal is afgegaan. Het afronden van de SWOT-analyse is niet gelukt vandaag.
