---
title: "Low-fid prototypes"
description: "Deze keer gaan we door met onderzoeken en worden de low-fid prototypes gevalideerd (of eigenlijk niet...)."
slug: "low-fid-prototype"
image: pic10.jpg
tags: ["enquete", "low-fid prototype", "interview", "valideren"]
categories: ["onderzoek"]
date: 2017-12-04
week: Week 4 | Dag 1
draft: false
---

De dag begon met het afmaken van het nieuwe low-fid prototype. Er zijn nog een paar last-minute wijzigingen geweest waaronder waar welke knop komt te staan en hoe de interactie verloopt. Vervolggens hebben we besloten om toch nog interviews af te gaan nemen in Den Haag, dus daar gaan we morgen met z'n drieën naar toe. Ter voorbereiding heb ik allereerst vragen opgesteld om deze kwijt te kunnen in de enquête. Na deze besproken te hebben, heb ik de enquête digitaal gemaakt, zodat we deze morgen op Den Haag Centraal snel kunnen invullen. Dit gaan we zelf doen, niet de ondervraagden. Ik heb de enquête interactief gemaakt op zo'n manier dat de gegeven antwoorden de hele vragenlijst beïnvloeden, zodat de lijst niet te lang wordt. Mensen willen in het algemeen snel verder. Of we überhaupt mensen kunnen activeren om de lijst in te vullen, valt nog te bezien.

Hierna ben ik naar een validatiemoment gegaan voor de low-fid prototypes. Het was lang wachten, maar uiteindelijk hebben we wel feedback gehad. Het product is niet gevalideerd. Wel konden we ermee uit de voeten en was duidelijk wat hoofdzaak en wat bijzaak is. Ook hebben we een beetje inzicht gekregen in fouten in 't concept m.b.t. kortingen en vouchers wat het hele businessmodel van PAARD zou aanpassen.

Als afsluiting heb ik de enquête nog afgerond en zo ingesteld dat alle ingevulde gegevens direct naar onze e-mail worden doorgestuurd. Zo is alles op één plek te vinden.